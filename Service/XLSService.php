<?php

namespace Arkulpa\UtilsBundle\Service;

use Arkulpa\AuthBundle\Entity\PushToken;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class XLSService
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }


    public function writeXLS($headers, $data, $filename)
    {
        $xlsrow = 1;
        $phpExcelObject = $this->container->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->setActiveSheetIndex(0);
        $phpExcelObject->getActiveSheet()->setTitle('Sheet 1');


        if ($data != null) {
            $i = 0;
            foreach ($headers as $col) {
                $phpExcelObject->getActiveSheet()->setCellValueExplicitByColumnAndRow(
                    $i,
                    $xlsrow,
                    $col['name'],
                    \PHPExcel_Cell_DataType::TYPE_STRING
                );
                $i++;
            }

            $xlsrow++;

            foreach ($data as $row) {
                $i = 0;
                for ($j = 0; $j < count($row); $j++) {
                    if (isset($row[$headers[$j]['key']])) {
                        $field = $row[$headers[$j]['key']];

                        if (isset($headers[$j]['convert'])) {
                            $field = $this->convertValue($field, $headers[$j]['convert']);
                        }

                        $phpExcelObject->getActiveSheet()->setCellValueExplicitByColumnAndRow(
                            $i,
                            $xlsrow,
                            $field,
                            $headers[$j]['dataType']
                        );
                    }
                    $i++;
                }
                ++$xlsrow;
            }
        }
        $writer = $this->container->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        $response = $this->container->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename . '_' . date('Y-m-d') . '.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;

    }

    private function convertValue($value, $convertType)
    {
        if ($convertType === 'timestamp' && $value instanceof \DateTime) {
            $value = $value->format('Y-m-d H:i:s');
        } elseif ($convertType === 'float') {
            $value = floatval($value);
        }
        return $value;
    }


    public function readXLSFile(UploadedFile $file)
    {
        $inputFileType = \PHPExcel_IOFactory::identify($file->getPath() . '/' . $file->getFilename());
        $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file->getPath() . '/' . $file->getFilename());

        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $rowData = [];
        for ($row = 1; $row <= $highestRow; $row++) {
            $rowData = array_merge(
                $rowData,
                $sheet->rangeToArray(
                    'A' . $row . ':' . $highestColumn . $row,
                    NULL,
                    TRUE,
                    FALSE
                )
            );
        }
        return $rowData;
    }
}
