<?php

namespace Arkulpa\UtilsBundle\Service;

use Arkulpa\UtilsBundle\Entity\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ImageService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @return Image
     */
    public function uploadImage(Request $request)
    {

        $date = new \DateTime();
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        $path = $date->format('Y/m');
        $fileName = time() . '_' . $file->getClientOriginalName();

        $completePath = $this->container->get('kernel')->getRootDir() . '/../uploads/' . $path;
        if (!is_dir($completePath)) {
            mkdir($completePath, 0777, true);
        }

        $file->move($completePath, $fileName);

        $title = rtrim(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName()), '.');

        $img = new Image();
        $img->setTitle($title);
        $img->setPath($path . '/' . $fileName);
        $img->setInsertTs($date);

        return $img;
    }


    public function resolveImagePath($path, $format)
    {
        $imagineCacheManager = $this->container->get('liip_imagine.cache.manager');
        return $imagineCacheManager->getBrowserPath($path, $format);

    }

}
