<?php

namespace Arkulpa\UtilsBundle\Service;

use Arkulpa\AuthBundle\Entity\PushToken;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PushService
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function sendPush(PushToken $pushToken, $type, $message, $title, $badge = 0, $subtitle = null, $tickerText = null, $vibrate = 1, $sound = 1)
    {
        if ($pushToken->getType() == 'A') {
            $data = array(
                'title' => $title,
                'subtitle' => $subtitle,
                'tickerText' => $tickerText,
                'vibrate' => $vibrate,
                'sound' => $sound,
                'type' => $type,
            );

            $messageObj = new AndroidMessage();
            $messageObj->setGCM(true);
            $messageObj->setData($data);
            $messageObj->setMessage($message);
        } else {
            $messageObj = new iOSMessage();
            $messageObj->setAPSBadge($badge);
            $messageObj->setDeviceIdentifier($pushToken->getToken());
            $data = new \stdClass();
            $data->title = $title;
            $data->body = $message;
            $messageObj->setMessage($data);
            $messageObj->setData(array('type' => $type));
        }

        $this->container->get('rms_push_notifications')->send($messageObj);
    }
}
