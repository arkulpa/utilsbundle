<?php

namespace Arkulpa\UtilsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * @ORM\MappedSuperclass
 */
class BaseEntity
{

    /**
     * @ORM\ManyToOne(targetEntity="Arkulpa\AuthBundle\Entity\User")
     * @ORM\JoinColumn(name="insertUser",nullable=false)
     * @Exclude()
     */
    private $insertUser;

    /**
     * @ORM\ManyToOne(targetEntity="Arkulpa\AuthBundle\Entity\User")
     * @ORM\JoinColumn(name="updateUser",nullable=true)
     * @Exclude()
     */
    private $updateUser;

    /**
     * @ORM\ManyToOne(targetEntity="Arkulpa\AuthBundle\Entity\User")
     * @ORM\JoinColumn(name="deleteUser",nullable=true)
     * @Exclude()
     */
    private $deleteUser;


    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="insertTs", type="datetime")
     */
    private $insertTs;

    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="updateTs", type="datetime", nullable=true)
     */
    private $updateTs;

    /**
     * @var \DateTime
     * @Exclude()
     * @ORM\Column(name="deleteTS", type="datetime", nullable=true)
     */
    private $deleteTs;

    /**
     * @var bool
     * @Exclude()
     * @ORM\Column(name="isDeleted", type="boolean")
     */
    private $is_deleted = false;


    /**
     * Set insertTs
     *
     * @param \DateTime $insertTs
     *
     * @return BaseEntity
     */
    public function setInsertTs($insertTs)
    {
        $this->insertTs = $insertTs;

        return $this;
    }

    /**
     * Get insertTs
     *
     * @return \DateTime
     */
    public function getInsertTs()
    {
        return $this->insertTs;
    }

    /**
     * Set updateTs
     *
     * @param \DateTime $updateTs
     *
     * @return BaseEntity
     */
    public function setUpdateTs($updateTs)
    {
        $this->updateTs = $updateTs;

        return $this;
    }

    /**
     * Get updateTs
     *
     * @return \DateTime
     */
    public function getUpdateTs()
    {
        return $this->updateTs;
    }

    /**
     * Set deleteTs
     *
     * @param \DateTime $deleteTs
     *
     * @return BaseEntity
     */
    public function setDeleteTs($deleteTs)
    {
        $this->deleteTs = $deleteTs;

        return $this;
    }

    /**
     * Get deleteTs
     *
     * @return \DateTime
     */
    public function getDeleteTs()
    {
        return $this->deleteTs;
    }

    /**
     * Set isDeleted
     *
     * @param boolean $isDeleted
     *
     * @return BaseEntity
     */
    public function setIsDeleted($isDeleted)
    {
        $this->is_deleted = $isDeleted;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Set insertUser
     *
     * @param \Arkulpa\AuthBundle\Entity\User $insertUser
     *
     * @return BaseEntity
     */
    public function setInsertUser(\Arkulpa\AuthBundle\Entity\User $insertUser)
    {
        $this->insertUser = $insertUser;
        $this->setInsertTs(new \DateTime());
        return $this;
    }

    /**
     * Get insertUser
     *
     * @return \Arkulpa\AuthBundle\Entity\User
     */
    public function getInsertUser()
    {
        return $this->insertUser;
    }

    /**
     * Set updateUser
     *
     * @param \Arkulpa\AuthBundle\Entity\User $updateUser
     *
     * @return BaseEntity
     */
    public function setUpdateUser(\Arkulpa\AuthBundle\Entity\User $updateUser = null)
    {
        $this->updateUser = $updateUser;
        $this->setUpdateTs(new \DateTime());
        return $this;
    }

    /**
     * Get updateUser
     *
     * @return \Arkulpa\AuthBundle\Entity\User
     */
    public function getUpdateUser()
    {
        return $this->updateUser;
    }

    /**
     * Set deleteUser
     *
     * @param \Arkulpa\AuthBundle\Entity\User $deleteUser
     *
     * @return BaseEntity
     */
    public function setDeleteUser(\Arkulpa\AuthBundle\Entity\User $deleteUser = null)
    {
        $this->deleteUser = $deleteUser;
        $this->setDeleteTs(new \DateTime());

        return $this;
    }

    /**
     * Get deleteUser
     *
     * @return \Arkulpa\AuthBundle\Entity\User
     */
    public function getDeleteUser()
    {
        return $this->deleteUser;
    }
}
