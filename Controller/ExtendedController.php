<?php

namespace Arkulpa\UtilsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExtendedController extends Controller
{
    protected function generateSuccesResponse($data = null)
    {
        $returnValue = array('data' => $data);
        $response = new JsonResponse($returnValue);

        return $response;
    }

    protected function generateValidationErrorResponse($validationErrors)
    {
        $returnValue = array('error' => $validationErrors);
        $response = new JsonResponse($returnValue, 400);

        return $response;
    }

    protected function generateCredentialsErrorResponse($message)
    {
        $returnValue = array('error' => array('message' => $this->get('translator.default')->trans($message)));
        $response = new JsonResponse($returnValue, 400);

        return $response;
    }

    protected function generateLogicErrorResponse($e)
    {
        if ($e instanceof \Exception) {
            $translatedError = $this->get('translator.default')->trans($e->getMessage());
        } else {
            $translatedError = $this->get('translator.default')->trans($e);
        }
        $returnValue = array('error' => array('message' => $translatedError));
        $response = new JsonResponse($returnValue, 500);

        return $response;
    }
}
